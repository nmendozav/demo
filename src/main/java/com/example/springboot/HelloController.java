package com.example.springboot;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.google.gson.Gson; 

@RestController
public class HelloController implements WebMvcConfigurer{

	static List<Person> personasList;
	static {
		personasList = new ArrayList<Person>();
		personasList.add(new Person("18584223-1", "Nicolas", "Mendoza", "16-08-1993"));
	}
	
	@Value("${:classpath:/resource/build/index.html}")
    private Resource index;

	@GetMapping("/")
	public ResponseEntity<InputStreamResource> index() throws IOException {
		return ResponseEntity.ok(new InputStreamResource(index.getInputStream()));
	}
	
	@Value("${:classpath:/resource/build/static}")
    private Resource stati;
	
	@GetMapping("/static")
	public ResponseEntity stati() throws IOException {
		return ResponseEntity.ok(new InputStreamResource(stati.getInputStream()));
	}
	
	@Value("${:classpath:/resource/build/static/css/main.cec7cf83.chunk.css}")
    private Resource mainchunk;
	
	@GetMapping("/static/css/main.cec7cf83.chunk.css")
	public ResponseEntity mainchunk() throws IOException {
		return ResponseEntity.ok(new InputStreamResource(mainchunk.getInputStream()));
	}

	
	
	@Value("${:classpath:/resource/build/static/js/2.50f50ab1.chunk.js}")
    private Resource jsdata;
	
	@GetMapping("/static/js/2.50f50ab1.chunk.js")
	public ResponseEntity jsdata() throws IOException {
		return ResponseEntity.ok(new InputStreamResource(jsdata.getInputStream()));
	}

	
	@Value("${:classpath:/resource/build/static/js/main.81726c46.chunk.js}")
    private Resource chunkdata2;
	
	@GetMapping("/static/js/main.81726c46.chunk.js")
	public ResponseEntity chunkdata2() throws IOException {
		return ResponseEntity.ok(new InputStreamResource(chunkdata2.getInputStream()));
	}


	@Value("${:classpath:/resource/build/manifest.json}")
    private Resource manifest;
	
	@GetMapping("/manifest.json")
	public ResponseEntity manifest() throws IOException {
		return ResponseEntity.ok(new InputStreamResource(manifest.getInputStream()));
	}
	
	
	
	@GetMapping("/person")
	public String getPerson(@RequestParam(value = "rut", defaultValue = "1-1") String rut) {
		System.out.println("PERSONAS LIST " + String.valueOf(personasList.size()));
		for (Person persona : personasList) {
	        if (persona.getRut().equals(rut)) {
	        	Gson gson = new Gson();
	            return gson.toJson(persona);
	        }
	    }
		return "No encontrado";
	}
	
	@PostMapping(value = "/createPerson", consumes = "application/json", produces = "application/json")
	public Person createPerson(@RequestBody Person person, HttpServletResponse response) {
	    personasList.add(person);
	    return person;
	}

}
