package com.example.springboot;

public class Person {

	private final String rut;
	private final String nombre;
	private final String apellido;
	private final String fechaNacimiento;
	
	/**
	 * @param rut
	 * @param nombre
	 * @param apellido
	 * @param fechaNacimiento
	 */
	public Person(String rut, String nombre, String apellido, String fechaNacimiento) {
		this.rut = rut;
		this.nombre = nombre;
		this.apellido = apellido;
		this.fechaNacimiento = fechaNacimiento;
	}
	
	public String getRut() {
		return rut;
	}
	public String getNombre() {
		return nombre;
	}
	public String getApellido() {
		return apellido;
	}
	public String getFechaNacimiento() {
		return fechaNacimiento;
	}
	
}